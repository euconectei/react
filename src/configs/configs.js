export const config = {
  INITIAL_PAGE: 1,
  ORGANIZATION: 'braziljs',
  PER_PAGE: 20,
  URL: {
    COMMITS: (org, repo, page, per_page) => `https://api.github.com/repos/${org}/${repo}/commits?page=${page}&per_page=${per_page}`,
    CONTRIBS: (org, repo) => `https://api.github.com/repos/${org}/${repo}/contributors`,
    REPOS: (org) => `https://api.github.com/users/${org}/repos`,
  }
};
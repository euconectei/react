import React, { Component } from 'react';
import { config } from '../../../configs/configs';

class Contributors extends Component {

  constructor(props) {
    super(props);

    this.state = {
      contribs: [],
      count: null,
      isLoading: false,
      error: null,
    };

    this.org = config.ORGANIZATION;
    this.url = config.URL.CONTRIBS(this.org, this.props.project);
  }

  componentDidMount() {
    this.setState({ isLoading: true });

    fetch(this.url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Erro ao buscar dados...');
        }
      })
      .then((contribs) => this.setState({ count: Object.keys(contribs).length, isLoading: false }))
      .catch((error) => this.setState({ error, isLoading: false }));
  }

  render() {
    const { isLoading, count } = this.state;

    if (isLoading) {
      return <div>Loading ...</div>;
    }
    return <div key={ count }>contribs { count }</div>;
  }
}

export default Contributors;
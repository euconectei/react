import React, { Component } from 'react';

class Forks extends Component {

  render() {
    return <div>forks { this.props.forks }</div>;
  }
}

export default Forks;
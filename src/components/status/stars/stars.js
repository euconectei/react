import React, { Component } from 'react';

class Stars extends Component {

  render() {
    return <div>stars { this.props.stars }</div>;
  }
}

export default Stars;
import React, { Component } from 'react';

class Commit extends Component {

  constructor(props) {
    super(props);

    this.state = {
      commit: [],
      isLoading: false,
      error: null,
    }

    this.date = new Date(this.props.date);
  }

  render() {
    const { isLoading } = this.state;

    if (isLoading) {
      return <li className="commits-list-item">Loading ...</li>;
    }

    return <li className="commits-list-item">
      <img src={ this.props.image } alt={ this.props.username } className="rounded-border commit-data-image" width="40" height="40" />
      <div className="commit-data">
        <p className="commit-data-title">{ this.props.title }</p>
        <p className="commit-data-user">@{ this.props.username }</p>
      </div>
      <span className="commit-data-date">{ this.date.getDay() + '/' + this.date.getMonth() + '/' + this.date.getFullYear() }</span>
    </li>
  }
}

export default Commit;
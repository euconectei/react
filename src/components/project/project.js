import React, { Component } from 'react';
import { config } from '../../configs/configs';
import Commit from '../commit/commit';
import Contributors from '../status/contribs/contribs';
import Forks from '../status/forks/forks';
import Stars from '../status/stars/stars';

class Project extends Component {

  constructor(props) {
    super(props);

    this.state = {
      commits: [],
      isLoading: false,
      error: null,
      lastItem: false,
    }

    this._loadMoreItems = this._loadMoreItems.bind(this);

    this.page = config.INITIAL_PAGE;
    this.per_page = config.PER_PAGE;
    this.url = config.URL.COMMITS(this.props.repo.name, this.page, this.per_page);
    console.log('url', this.url);
  }

  _getInitialState() {
    this.setState({ isLoading: true });
    console.log(this.props.repo.name);

    fetch(config.URL.COMMITS(config.ORGANIZATION, this.props.repo.name, this.page, this.per_page))
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Erro ao buscar dados...');
        }
      })
      .then(commits => {
        if (commits.length < this.per_page) {
          this.setState({ commits, isLoading: false, lastItem: true })
        } else {
          this.setState({ commits, isLoading: false, });
        }
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }

  _loadMoreItems() {
    this.setState({ isLoading: true });
    this.page = this.page + 1;

    fetch(config.URL.COMMITS(config.ORGANIZATION, this.props.repo.name, this.page, this.per_page))
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Erro ao buscar dados...');
        }
      })
      .then(newCommits => {
        if (newCommits.length < this.per_page) {
          this.setState({ commits: this.state.commits.concat(newCommits), isLoading: false, lastItem: true });
        } else {
          this.setState({ commits: this.state.commits.concat(newCommits), isLoading: false });
        }
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }

  _renderItems() {
    try {
      return this.state.commits.map((item) => <Commit key={item.sha} title={item.commit.message} username={(item.author) ? item.author.login : item.commit.committer.name} date={item.commit.author.date} image={(item.author) ? item.author.avatar_url : 'https://github.com/octocat.png'} />);
    } catch (error) {
      throw new Error(error);
    }
  }

  componentDidMount() {
    this._getInitialState();
  }

  render() {
     return <div>
      <section className="status">
        <Stars stars={ this.props.repo.stargazers_count } />
        <Forks forks={ this.props.repo.forks } />
        <Contributors project={ this.props.repo.name } />
      </section>
      <section className="commits">
        <ul className="commits-list">
          {this._renderItems()}
        </ul>
        {this.state.lastItem ? null : <a role="button" onClick={this._loadMoreItems} className="more" >mais commits</a>}
      </section>
    </div>
  };
}
export default Project;
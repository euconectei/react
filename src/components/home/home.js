import React, { Component } from 'react';

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      projects: [],
      isLoading: false,
      error: null,
    }
  }

  render() {
    const { isLoading } = this.state;

    if (isLoading) {
      return <div><section className="status">Loading ...</section></div>;
    }

    return <section className="projects">
      <ul className="projects-list">
        {this.props.repos.map((item, i) => {
          return <li key={i} className="projects-list-item">
            <div className="project-data">
              <p className="project-data-title">
                <a href={'/' + item.name}>{item.name}</a>
              </p>
              <p className="project-data-description">{item.description}</p>
            </div>
          </li>
        })}
      </ul>
    </section>
  }
}

export default Home;
import React, { Component } from 'react';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Home from './components/home/home';
import Project from './components/project/project';
import { config } from './configs/configs';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      repos: [],
      isLoading: false,
      error: null,
    };

    this.org = config.ORGANIZATION;
    this.url = config.URL.REPOS(this.org);

  }

  componentDidMount() {
    console.log(this.url);
    fetch(this.url)
      .then(response => {
        this.setState({ isLoading: true });

        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Erro ao buscar dados...');
        }
      })
      .then(repos => {
        repos.sort((a, b) => {
          if (a.stargazers_count < b.stargazers_count) { return 1; }
          if (a.stargazers_count > b.stargazers_count) { return -1; }
          return 0;
        });
        this.setState({ repos, isLoading: false })
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { isLoading } = this.state;

    if (isLoading) {
      return <div className="container">Loading ...</div>;
    }

    return (
      <Router>
        <div className="container">
          <nav className="menu">
            <h1 className="page-title"><Link to="/">projetos</Link></h1>
            <ul>
              {this.state.repos.map((repo, i) => <li key={i}><Link to={repo.name}>{ repo.name }</Link></li>) }
            </ul>
          </nav>
          <main className="content">
            <Route exact path="/" render={(props) => <Home repos={this.state.repos} />} />
            {this.state.repos.map((repo) => <Route exact key={ repo.sha } path={"/" + repo.name} render={(props) => (<Project repo={repo} />)} />) }
          </main>
        </div>
      </Router>
    );
  }
}

export default App;
